package com.example.cinepolis

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Buy tickets
        val btBuy = findViewById<Button>(R.id.button)

        btBuy?.setOnClickListener {
            Toast.makeText(this, "Lo sentimos se han agotado las entradas.", Toast.LENGTH_LONG).show()
        }

        // Change image
        val ivAntman = findViewById<ImageView>(R.id.imageView5)

        ivAntman.setOnClickListener {
            ivAntman.setImageDrawable(Drawable.createFromPath("res/drawable/f85d3dda6f8b0c015130524696316169.jpg"))
        }

        // Finish screen
        val tvBottom = findViewById<TextView>(R.id.textView2)

        tvBottom.setOnClickListener {
            finish()
        }

    }
}